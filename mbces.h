#define MB_CM_NNODES_CL (4U)
#define MB_WCV_NDESTS_CL (15U)
extern mb_char_map_t mb_cm_CL;
extern mb_char_node_t mb_cm_nodev_CL[];
extern mb_wchar_range_t mb_cm_dectab_CL[];
extern size_t mb_cm_dectab_bv_CL[];
extern mb_wchar_range_t mb_wcv_CL[];
#define MB_CM_NNODES_GL (1U)
#define MB_WCV_NDESTS_GL (1U)
extern mb_char_map_t mb_cm_GL;
extern mb_char_node_t mb_cm_nodev_GL[];
extern mb_wchar_range_t mb_cm_dectab_GL[];
extern size_t mb_cm_dectab_bv_GL[];
extern mb_wchar_range_t mb_wcv_GL[];
#define MB_CM_NNODES_94x94L (1U)
#define MB_WCV_NDESTS_94x94L (1U)
extern mb_char_map_t mb_cm_94x94L;
extern mb_char_node_t mb_cm_nodev_94x94L[];
extern mb_wchar_range_t mb_cm_dectab_94x94L[];
extern size_t mb_cm_dectab_bv_94x94L[];
extern mb_wchar_range_t mb_wcv_94x94L[];
#define MB_CM_NNODES_96L (1U)
#define MB_WCV_NDESTS_96L (1U)
extern mb_char_map_t mb_cm_96L;
extern mb_char_node_t mb_cm_nodev_96L[];
extern mb_wchar_range_t mb_cm_dectab_96L[];
extern size_t mb_cm_dectab_bv_96L[];
extern mb_wchar_range_t mb_wcv_96L[];
#define MB_CM_NNODES_94L (1U)
#define MB_WCV_NDESTS_94L (1U)
extern mb_char_map_t mb_cm_94L;
extern mb_char_node_t mb_cm_nodev_94L[];
extern mb_wchar_range_t mb_cm_dectab_94L[];
extern size_t mb_cm_dectab_bv_94L[];
extern mb_wchar_range_t mb_wcv_94L[];
#define MB_CM_NNODES_GR (2U)
#define MB_WCV_NDESTS_GR (2U)
extern mb_char_map_t mb_cm_GR;
extern mb_char_node_t mb_cm_nodev_GR[];
extern mb_wchar_range_t mb_cm_dectab_GR[];
extern size_t mb_cm_dectab_bv_GR[];
extern mb_wchar_range_t mb_wcv_GR[];
#define MB_CM_NNODES_94x94R (1U)
#define MB_WCV_NDESTS_94x94R (1U)
extern mb_char_map_t mb_cm_94x94R;
extern mb_char_node_t mb_cm_nodev_94x94R[];
extern mb_wchar_range_t mb_cm_dectab_94x94R[];
extern size_t mb_cm_dectab_bv_94x94R[];
extern mb_wchar_range_t mb_wcv_94x94R[];
#define MB_CM_NNODES_96R (1U)
#define MB_WCV_NDESTS_96R (1U)
extern mb_char_map_t mb_cm_96R;
extern mb_char_node_t mb_cm_nodev_96R[];
extern mb_wchar_range_t mb_cm_dectab_96R[];
extern size_t mb_cm_dectab_bv_96R[];
extern mb_wchar_range_t mb_wcv_96R[];
#define MB_CM_NNODES_94R (1U)
#define MB_WCV_NDESTS_94R (1U)
extern mb_char_map_t mb_cm_94R;
extern mb_char_node_t mb_cm_nodev_94R[];
extern mb_wchar_range_t mb_cm_dectab_94R[];
extern size_t mb_cm_dectab_bv_94R[];
extern mb_wchar_range_t mb_wcv_94R[];
#define MB_CM_NNODES_CLGL (1U)
#define MB_WCV_NDESTS_CLGL (1U)
extern mb_char_map_t mb_cm_CLGL;
extern mb_char_node_t mb_cm_nodev_CLGL[];
extern mb_wchar_range_t mb_cm_dectab_CLGL[];
extern size_t mb_cm_dectab_bv_CLGL[];
extern mb_wchar_range_t mb_wcv_CLGL[];
#define MB_CM_NNODES_ASCII (1U)
#define MB_WCV_NDESTS_ASCII (1U)
extern mb_char_map_t mb_cm_ASCII;
extern mb_char_node_t mb_cm_nodev_ASCII[];
extern mb_wchar_range_t mb_cm_dectab_ASCII[];
extern size_t mb_cm_dectab_bv_ASCII[];
extern mb_wchar_range_t mb_wcv_ASCII[];
#define MB_CM_NNODES_UTF8 (3U)
#define MB_WCV_NDESTS_UTF8 (3U)
extern mb_char_map_t mb_cm_UTF8;
extern mb_char_node_t mb_cm_nodev_UTF8[];
extern mb_wchar_range_t mb_cm_dectab_UTF8[];
extern size_t mb_cm_dectab_bv_UTF8[];
extern mb_wchar_range_t mb_wcv_UTF8[];
#define MB_CM_NNODES_UTF16 (1U)
#define MB_WCV_NDESTS_UTF16 (1U)
extern mb_char_map_t mb_cm_UTF16;
extern mb_char_node_t mb_cm_nodev_UTF16[];
extern mb_wchar_range_t mb_cm_dectab_UTF16[];
extern size_t mb_cm_dectab_bv_UTF16[];
extern mb_wchar_range_t mb_wcv_UTF16[];
#define MB_CM_NNODES_UTF16BE (1U)
#define MB_WCV_NDESTS_UTF16BE (1U)
extern mb_char_map_t mb_cm_UTF16BE;
extern mb_char_node_t mb_cm_nodev_UTF16BE[];
extern mb_wchar_range_t mb_cm_dectab_UTF16BE[];
extern size_t mb_cm_dectab_bv_UTF16BE[];
extern mb_wchar_range_t mb_wcv_UTF16BE[];
#define MB_CM_NNODES_UTF16LE (1U)
#define MB_WCV_NDESTS_UTF16LE (1U)
extern mb_char_map_t mb_cm_UTF16LE;
extern mb_char_node_t mb_cm_nodev_UTF16LE[];
extern mb_wchar_range_t mb_cm_dectab_UTF16LE[];
extern size_t mb_cm_dectab_bv_UTF16LE[];
extern mb_wchar_range_t mb_wcv_UTF16LE[];
#define MB_CM_NNODES_MOEINTERNAL (1U)
#define MB_WCV_NDESTS_MOEINTERNAL (1U)
extern mb_char_map_t mb_cm_MOEINTERNAL;
extern mb_char_node_t mb_cm_nodev_MOEINTERNAL[];
extern mb_wchar_range_t mb_cm_dectab_MOEINTERNAL[];
extern size_t mb_cm_dectab_bv_MOEINTERNAL[];
extern mb_wchar_range_t mb_wcv_MOEINTERNAL[];
#define MB_ENCMAP_NNODES_ISO2022 (7U)
extern mb_encoder_t mb_encmap_nodev_ISO2022[];
extern short mb_encmap_iv_ISO2022[];
extern mb_encoder_map_t mb_encmap_ISO2022;
#define MB_ENCMAP_NNODES_UTF8 (8U)
extern mb_encoder_t mb_encmap_nodev_UTF8[];
extern short mb_encmap_iv_UTF8[];
extern mb_encoder_map_t mb_encmap_UTF8;
#define MB_ENCMAP_NNODES_UTF16 (1U)
extern mb_encoder_t mb_encmap_nodev_UTF16[];
extern short mb_encmap_iv_UTF16[];
extern mb_encoder_map_t mb_encmap_UTF16;
#define MB_ENCMAP_NNODES_UTF16BE (1U)
extern mb_encoder_t mb_encmap_nodev_UTF16BE[];
extern short mb_encmap_iv_UTF16BE[];
extern mb_encoder_map_t mb_encmap_UTF16BE;
#define MB_ENCMAP_NNODES_UTF16LE (1U)
extern mb_encoder_t mb_encmap_nodev_UTF16LE[];
extern short mb_encmap_iv_UTF16LE[];
extern mb_encoder_map_t mb_encmap_UTF16LE;
#define MB_ENCMAP_NNODES_MOEINTERNAL (6U)
extern mb_encoder_t mb_encmap_nodev_MOEINTERNAL[];
extern short mb_encmap_iv_MOEINTERNAL[];
extern mb_encoder_map_t mb_encmap_MOEINTERNAL;
#define MB_DECMAP_TABSIZE_1 (0U)
extern mb_decoder_map_t mb_decmap_ASCII;
extern mb_ces_t mb_ces_ASCII;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_GB2312;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_GB2312(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_GB2312(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_GB2312;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_GB_ISO_IR_165;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_GB_ISO_IR_165(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_GB_ISO_IR_165(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_GB_ISO_IR_165;
#define MB_DECMAP_TABSIZE_4 (0U)
extern mb_decoder_map_t mb_decmap_CN;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_CN(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_CN(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_CN;
#define MB_DECMAP_TABSIZE_10 (0U)
extern mb_decoder_map_t mb_decmap_CN_EXT;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_CN_EXT(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_CN_EXT(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_CN_EXT;
#define MB_DECMAP_TABSIZE_4 (0U)
extern mb_decoder_map_t mb_decmap_EUC_JP;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_EUC_JP(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_EUC_JP(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_EUC_JP;
#define MB_DECMAP_TABSIZE_4 (0U)
extern mb_decoder_map_t mb_decmap_EUC_JISX0213;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_EUC_JISX0213(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_EUC_JISX0213(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_EUC_JISX0213;
#define MB_DECMAP_TABSIZE_4 (0U)
extern mb_decoder_map_t mb_decmap_ISO2022JP;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO2022JP(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO2022JP(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO2022JP;
#define MB_DECMAP_TABSIZE_5 (0U)
extern mb_decoder_map_t mb_decmap_ISO2022JP2;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO2022JP2(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO2022JP2(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO2022JP2;
#define MB_DECMAP_TABSIZE_5 (0U)
extern mb_decoder_map_t mb_decmap_ISO2022JP3;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO2022JP3(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO2022JP3(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO2022JP3;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_EUC_KR;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_EUC_KR(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_EUC_KR(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_EUC_KR;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO2022KR;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO2022KR(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO2022KR(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO2022KR;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_1;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_1(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_1(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_1;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_2;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_2(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_2(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_2;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_3;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_3(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_3(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_3;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_4;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_4(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_4(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_4;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_5;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_5(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_5(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_5;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_6;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_6(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_6(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_6;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_7;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_7(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_7(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_7;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_8;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_8(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_8(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_8;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_9;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_9(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_9(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_9;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_ISO8859_10;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_ISO8859_10(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_ISO8859_10(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_ISO8859_10;
#define MB_DECMAP_TABSIZE_18 (0U)
extern mb_decoder_map_t mb_decmap_XCTEXT;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_XCTEXT(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_XCTEXT(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_XCTEXT;
#define MB_DECMAP_TABSIZE_4 (0U)
extern mb_decoder_map_t mb_decmap_UTF8;
extern mb_ces_t mb_ces_UTF8;
#define MB_DECMAP_TABSIZE_1 (0U)
extern mb_decoder_map_t mb_decmap_UTF16;
extern mb_ces_t mb_ces_UTF16;
#define MB_DECMAP_TABSIZE_1 (0U)
extern mb_decoder_map_t mb_decmap_UTF16BE;
extern mb_ces_t mb_ces_UTF16BE;
#define MB_DECMAP_TABSIZE_1 (0U)
extern mb_decoder_map_t mb_decmap_UTF16LE;
extern mb_ces_t mb_ces_UTF16LE;
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_MOEINTERNAL;
extern mb_ces_t mb_ces_MOEINTERNAL;
#ifdef USE_KOI8R
#define MB_CM_NNODES_KOI8R (1U)
#define MB_WCV_NDESTS_KOI8R (1U)
extern mb_char_map_t mb_cm_KOI8R;
extern mb_char_node_t mb_cm_nodev_KOI8R[];
extern mb_wchar_range_t mb_cm_dectab_KOI8R[];
extern size_t mb_cm_dectab_bv_KOI8R[];
extern mb_wchar_range_t mb_wcv_KOI8R[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_KOI8R;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_KOI8R(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_KOI8R(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_KOI8R;
#define MB_ENCMAP_NNODES_KOI8R (6U)
extern mb_encoder_t mb_encmap_nodev_KOI8R[];
extern short mb_encmap_iv_KOI8R[];
extern mb_encoder_map_t mb_encmap_KOI8R;
#define MB_KOI8R_FC (0x42)
#define MB_KOI8R_FC_MAX (0x42)
#endif
#ifdef USE_KOI8U
#define MB_CM_NNODES_KOI8U (1U)
#define MB_WCV_NDESTS_KOI8U (1U)
extern mb_char_map_t mb_cm_KOI8U;
extern mb_char_node_t mb_cm_nodev_KOI8U[];
extern mb_wchar_range_t mb_cm_dectab_KOI8U[];
extern size_t mb_cm_dectab_bv_KOI8U[];
extern mb_wchar_range_t mb_wcv_KOI8U[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_KOI8U;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_KOI8U(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_KOI8U(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_KOI8U;
#define MB_ENCMAP_NNODES_KOI8U (6U)
extern mb_encoder_t mb_encmap_nodev_KOI8U[];
extern short mb_encmap_iv_KOI8U[];
extern mb_encoder_map_t mb_encmap_KOI8U;
#define MB_KOI8U_FC (0x43)
#define MB_KOI8U_FC_MAX (0x43)
#endif
#ifdef USE_WIN1250
#define MB_CM_NNODES_WIN1250 (1U)
#define MB_WCV_NDESTS_WIN1250 (1U)
extern mb_char_map_t mb_cm_WIN1250;
extern mb_char_node_t mb_cm_nodev_WIN1250[];
extern mb_wchar_range_t mb_cm_dectab_WIN1250[];
extern size_t mb_cm_dectab_bv_WIN1250[];
extern mb_wchar_range_t mb_wcv_WIN1250[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1250;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1250(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1250(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1250;
#define MB_ENCMAP_NNODES_WIN1250 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1250[];
extern short mb_encmap_iv_WIN1250[];
extern mb_encoder_map_t mb_encmap_WIN1250;
#define MB_WIN1250_FC (0x44)
#define MB_WIN1250_FC_MAX (0x44)
#endif
#ifdef USE_WIN1251
#define MB_CM_NNODES_WIN1251 (1U)
#define MB_WCV_NDESTS_WIN1251 (1U)
extern mb_char_map_t mb_cm_WIN1251;
extern mb_char_node_t mb_cm_nodev_WIN1251[];
extern mb_wchar_range_t mb_cm_dectab_WIN1251[];
extern size_t mb_cm_dectab_bv_WIN1251[];
extern mb_wchar_range_t mb_wcv_WIN1251[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1251;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1251(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1251(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1251;
#define MB_ENCMAP_NNODES_WIN1251 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1251[];
extern short mb_encmap_iv_WIN1251[];
extern mb_encoder_map_t mb_encmap_WIN1251;
#define MB_WIN1251_FC (0x45)
#define MB_WIN1251_FC_MAX (0x45)
#endif
#ifdef USE_WIN1252
#define MB_CM_NNODES_WIN1252 (1U)
#define MB_WCV_NDESTS_WIN1252 (1U)
extern mb_char_map_t mb_cm_WIN1252;
extern mb_char_node_t mb_cm_nodev_WIN1252[];
extern mb_wchar_range_t mb_cm_dectab_WIN1252[];
extern size_t mb_cm_dectab_bv_WIN1252[];
extern mb_wchar_range_t mb_wcv_WIN1252[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1252;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1252(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1252(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1252;
#define MB_ENCMAP_NNODES_WIN1252 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1252[];
extern short mb_encmap_iv_WIN1252[];
extern mb_encoder_map_t mb_encmap_WIN1252;
#define MB_WIN1252_FC (0x46)
#define MB_WIN1252_FC_MAX (0x46)
#endif
#ifdef USE_WIN1253
#define MB_CM_NNODES_WIN1253 (1U)
#define MB_WCV_NDESTS_WIN1253 (1U)
extern mb_char_map_t mb_cm_WIN1253;
extern mb_char_node_t mb_cm_nodev_WIN1253[];
extern mb_wchar_range_t mb_cm_dectab_WIN1253[];
extern size_t mb_cm_dectab_bv_WIN1253[];
extern mb_wchar_range_t mb_wcv_WIN1253[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1253;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1253(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1253(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1253;
#define MB_ENCMAP_NNODES_WIN1253 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1253[];
extern short mb_encmap_iv_WIN1253[];
extern mb_encoder_map_t mb_encmap_WIN1253;
#define MB_WIN1253_FC (0x47)
#define MB_WIN1253_FC_MAX (0x47)
#endif
#ifdef USE_WIN1254
#define MB_CM_NNODES_WIN1254 (1U)
#define MB_WCV_NDESTS_WIN1254 (1U)
extern mb_char_map_t mb_cm_WIN1254;
extern mb_char_node_t mb_cm_nodev_WIN1254[];
extern mb_wchar_range_t mb_cm_dectab_WIN1254[];
extern size_t mb_cm_dectab_bv_WIN1254[];
extern mb_wchar_range_t mb_wcv_WIN1254[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1254;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1254(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1254(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1254;
#define MB_ENCMAP_NNODES_WIN1254 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1254[];
extern short mb_encmap_iv_WIN1254[];
extern mb_encoder_map_t mb_encmap_WIN1254;
#define MB_WIN1254_FC (0x48)
#define MB_WIN1254_FC_MAX (0x48)
#endif
#ifdef USE_WIN1255
#define MB_CM_NNODES_WIN1255 (1U)
#define MB_WCV_NDESTS_WIN1255 (1U)
extern mb_char_map_t mb_cm_WIN1255;
extern mb_char_node_t mb_cm_nodev_WIN1255[];
extern mb_wchar_range_t mb_cm_dectab_WIN1255[];
extern size_t mb_cm_dectab_bv_WIN1255[];
extern mb_wchar_range_t mb_wcv_WIN1255[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1255;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1255(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1255(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1255;
#define MB_ENCMAP_NNODES_WIN1255 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1255[];
extern short mb_encmap_iv_WIN1255[];
extern mb_encoder_map_t mb_encmap_WIN1255;
#define MB_WIN1255_FC (0x49)
#define MB_WIN1255_FC_MAX (0x49)
#endif
#ifdef USE_WIN1256
#define MB_CM_NNODES_WIN1256 (1U)
#define MB_WCV_NDESTS_WIN1256 (1U)
extern mb_char_map_t mb_cm_WIN1256;
extern mb_char_node_t mb_cm_nodev_WIN1256[];
extern mb_wchar_range_t mb_cm_dectab_WIN1256[];
extern size_t mb_cm_dectab_bv_WIN1256[];
extern mb_wchar_range_t mb_wcv_WIN1256[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1256;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1256(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1256(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1256;
#define MB_ENCMAP_NNODES_WIN1256 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1256[];
extern short mb_encmap_iv_WIN1256[];
extern mb_encoder_map_t mb_encmap_WIN1256;
#define MB_WIN1256_FC (0x4A)
#define MB_WIN1256_FC_MAX (0x4A)
#endif
#ifdef USE_WIN1257
#define MB_CM_NNODES_WIN1257 (1U)
#define MB_WCV_NDESTS_WIN1257 (1U)
extern mb_char_map_t mb_cm_WIN1257;
extern mb_char_node_t mb_cm_nodev_WIN1257[];
extern mb_wchar_range_t mb_cm_dectab_WIN1257[];
extern size_t mb_cm_dectab_bv_WIN1257[];
extern mb_wchar_range_t mb_wcv_WIN1257[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1257;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1257(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1257(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1257;
#define MB_ENCMAP_NNODES_WIN1257 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1257[];
extern short mb_encmap_iv_WIN1257[];
extern mb_encoder_map_t mb_encmap_WIN1257;
#define MB_WIN1257_FC (0x4B)
#define MB_WIN1257_FC_MAX (0x4B)
#endif
#ifdef USE_WIN1258
#define MB_CM_NNODES_WIN1258 (1U)
#define MB_WCV_NDESTS_WIN1258 (1U)
extern mb_char_map_t mb_cm_WIN1258;
extern mb_char_node_t mb_cm_nodev_WIN1258[];
extern mb_wchar_range_t mb_cm_dectab_WIN1258[];
extern size_t mb_cm_dectab_bv_WIN1258[];
extern mb_wchar_range_t mb_wcv_WIN1258[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_WIN1258;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_WIN1258(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_WIN1258(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_WIN1258;
#define MB_ENCMAP_NNODES_WIN1258 (6U)
extern mb_encoder_t mb_encmap_nodev_WIN1258[];
extern short mb_encmap_iv_WIN1258[];
extern mb_encoder_map_t mb_encmap_WIN1258;
#define MB_WIN1258_FC (0x4C)
#define MB_WIN1258_FC_MAX (0x4C)
#endif
#ifdef USE_SJIS
#define MB_CM_NNODES_SJIS (3U)
#define MB_WCV_NDESTS_SJIS (2U)
extern mb_char_map_t mb_cm_SJIS;
extern mb_char_node_t mb_cm_nodev_SJIS[];
extern mb_wchar_range_t mb_cm_dectab_SJIS[];
extern size_t mb_cm_dectab_bv_SJIS[];
extern mb_wchar_range_t mb_wcv_SJIS[];
#define MB_DECMAP_TABSIZE_3 (0U)
extern mb_decoder_map_t mb_decmap_SJIS;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_SJIS(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_SJIS(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_SJIS;
#define MB_ENCMAP_NNODES_SJIS (8U)
extern mb_encoder_t mb_encmap_nodev_SJIS[];
extern short mb_encmap_iv_SJIS[];
extern mb_encoder_map_t mb_encmap_SJIS;
#endif
#ifdef USE_SJIS0213
#define MB_CM_NNODES_SJIS0213 (9U)
#define MB_WCV_NDESTS_SJIS0213 (11U)
extern mb_char_map_t mb_cm_SJIS0213;
extern mb_char_node_t mb_cm_nodev_SJIS0213[];
extern mb_wchar_range_t mb_cm_dectab_SJIS0213[];
extern size_t mb_cm_dectab_bv_SJIS0213[];
extern mb_wchar_range_t mb_wcv_SJIS0213[];
#define MB_DECMAP_TABSIZE_12 (0U)
extern mb_decoder_map_t mb_decmap_SJIS0213;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_SJIS0213(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_SJIS0213(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_SJIS0213;
#define MB_ENCMAP_NNODES_SJIS0213 (14U)
extern mb_encoder_t mb_encmap_nodev_SJIS0213[];
extern short mb_encmap_iv_SJIS0213[];
extern mb_encoder_map_t mb_encmap_SJIS0213;
#endif
#ifdef USE_BIG5
#define MB_CM_NNODES_BIG5 (1U)
#define MB_WCV_NDESTS_BIG5 (1U)
extern mb_char_map_t mb_cm_BIG5;
extern mb_char_node_t mb_cm_nodev_BIG5[];
extern mb_wchar_range_t mb_cm_dectab_BIG5[];
extern size_t mb_cm_dectab_bv_BIG5[];
extern mb_wchar_range_t mb_wcv_BIG5[];
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_BIG5;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_BIG5(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_BIG5(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_BIG5;
#define MB_ENCMAP_NNODES_BIG5 (6U)
extern mb_encoder_t mb_encmap_nodev_BIG5[];
extern short mb_encmap_iv_BIG5[];
extern mb_encoder_map_t mb_encmap_BIG5;
#define MB_BIG5_FC (0x42)
#define MB_BIG5_FC_MAX (0x42)
#endif
#ifdef USE_JOHAB
#define MB_CM_NNODES_JOHAB (6U)
#define MB_WCV_NDESTS_JOHAB (6U)
extern mb_char_map_t mb_cm_JOHAB;
extern mb_char_node_t mb_cm_nodev_JOHAB[];
extern mb_wchar_range_t mb_cm_dectab_JOHAB[];
extern size_t mb_cm_dectab_bv_JOHAB[];
extern mb_wchar_range_t mb_wcv_JOHAB[];
#define MB_DECMAP_TABSIZE_7 (0U)
extern mb_decoder_map_t mb_decmap_JOHAB;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_JOHAB(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_JOHAB(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_JOHAB;
#define MB_ENCMAP_NNODES_JOHAB (11U)
extern mb_encoder_t mb_encmap_nodev_JOHAB[];
extern short mb_encmap_iv_JOHAB[];
extern mb_encoder_map_t mb_encmap_JOHAB;
#define MB_JOHAB_FC (0x43)
#define MB_JOHAB_FC_MAX (0x43)
#endif
#ifdef USE_UHANG
#define MB_CM_NNODES_UHANG (3U)
#define MB_WCV_NDESTS_UHANG (2U)
extern mb_char_map_t mb_cm_UHANG;
extern mb_char_node_t mb_cm_nodev_UHANG[];
extern mb_wchar_range_t mb_cm_dectab_UHANG[];
extern size_t mb_cm_dectab_bv_UHANG[];
extern mb_wchar_range_t mb_wcv_UHANG[];
#define MB_DECMAP_TABSIZE_3 (0U)
extern mb_decoder_map_t mb_decmap_UHANG;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_UHANG(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_UHANG(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_UHANG;
#define MB_ENCMAP_NNODES_UHANG (8U)
extern mb_encoder_t mb_encmap_nodev_UHANG[];
extern short mb_encmap_iv_UHANG[];
extern mb_encoder_map_t mb_encmap_UHANG;
#define MB_UHANG_FC (0x44)
#define MB_UHANG_FC_MAX (0x44)
#endif
#ifdef USE_EUC_TW
#define MB_CM_NNODES_EUC_TW (2U)
#define MB_WCV_NDESTS_EUC_TW (3U)
extern mb_char_map_t mb_cm_EUC_TW;
extern mb_char_node_t mb_cm_nodev_EUC_TW[];
extern mb_wchar_range_t mb_cm_dectab_EUC_TW[];
extern size_t mb_cm_dectab_bv_EUC_TW[];
extern mb_wchar_range_t mb_wcv_EUC_TW[];
#define MB_DECMAP_TABSIZE_4 (0U)
extern mb_decoder_map_t mb_decmap_EUC_TW;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_EUC_TW(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_EUC_TW(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_EUC_TW;
#define MB_ENCMAP_NNODES_EUC_TW (7U)
extern mb_encoder_t mb_encmap_nodev_EUC_TW[];
extern short mb_encmap_iv_EUC_TW[];
extern mb_encoder_map_t mb_encmap_EUC_TW;
#define MB_EUC_TW_FC (0x45)
#define MB_EUC_TW_FC_MAX (0x47)
#endif
#ifdef USE_EUC_JISX0213_PACKED
#define MB_CM_NNODES_EUC_JISX0213_PACKED (3U)
#define MB_WCV_NDESTS_EUC_JISX0213_PACKED (12U)
extern mb_char_map_t mb_cm_EUC_JISX0213_PACKED;
extern mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKED[];
extern mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKED[];
extern size_t mb_cm_dectab_bv_EUC_JISX0213_PACKED[];
extern mb_wchar_range_t mb_wcv_EUC_JISX0213_PACKED[];
#define MB_DECMAP_TABSIZE_13 (0U)
extern mb_decoder_map_t mb_decmap_EUC_JISX0213_PACKED;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_EUC_JISX0213_PACKED(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_EUC_JISX0213_PACKED(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_EUC_JISX0213_PACKED;
#define MB_ENCMAP_NNODES_EUC_JISX0213_PACKED (8U)
extern mb_encoder_t mb_encmap_nodev_EUC_JISX0213_PACKED[];
extern short mb_encmap_iv_EUC_JISX0213_PACKED[];
extern mb_encoder_map_t mb_encmap_EUC_JISX0213_PACKED;
#endif
#ifdef USE_GBK
#define MB_CM_NNODES_GBK (2U)
#define MB_WCV_NDESTS_GBK (2U)
extern mb_char_map_t mb_cm_GBK;
extern mb_char_node_t mb_cm_nodev_GBK[];
extern mb_wchar_range_t mb_cm_dectab_GBK[];
extern size_t mb_cm_dectab_bv_GBK[];
extern mb_wchar_range_t mb_wcv_GBK[];
#define MB_DECMAP_TABSIZE_3 (0U)
extern mb_decoder_map_t mb_decmap_GBK;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_GBK(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_GBK(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_GBK;
#define MB_ENCMAP_NNODES_GBK (7U)
extern mb_encoder_t mb_encmap_nodev_GBK[];
extern short mb_encmap_iv_GBK[];
extern mb_encoder_map_t mb_encmap_GBK;
#define MB_GBK_FC (0x48)
#define MB_GBK_FC_MAX (0x48)
#endif
#ifdef USE_GBK2K
#define MB_CM_NNODES_GBK2K (9U)
#define MB_WCV_NDESTS_GBK2K (16U)
extern mb_char_map_t mb_cm_GBK2K;
extern mb_char_node_t mb_cm_nodev_GBK2K[];
extern mb_wchar_range_t mb_cm_dectab_GBK2K[];
extern size_t mb_cm_dectab_bv_GBK2K[];
extern mb_wchar_range_t mb_wcv_GBK2K[];
#define MB_DECMAP_TABSIZE_17 (0U)
extern mb_decoder_map_t mb_decmap_GBK2K;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_GBK2K(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_GBK2K(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_GBK2K;
#define MB_ENCMAP_NNODES_GBK2K (14U)
extern mb_encoder_t mb_encmap_nodev_GBK2K[];
extern short mb_encmap_iv_GBK2K[];
extern mb_encoder_map_t mb_encmap_GBK2K;
#define MB_GBK2K_FC (0x49)
#define MB_GBK2K_FC_MAX (0x58)
#endif
#define MB_DECMAP_TABSIZE_2 (0U)
extern mb_decoder_map_t mb_decmap_TIS620;
#ifdef USE_UCS
extern mb_wchar_t mb_conv_to_TIS620(mb_wchar_t, mb_ces_t *);
extern size_t mb_conv_ws_to_TIS620(mb_wchar_t *, mb_wchar_t *, mb_info_t *);
#endif
extern mb_ces_t mb_ces_TIS620;
