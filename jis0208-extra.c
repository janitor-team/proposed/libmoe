#include "mb.h"

static unsigned int j2u[] = {
#include "jis0208-to-ucs-extra.h"
};

size_t
mb_jisx0208_to_ucs_extra(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
  size_t n;
  unsigned int wc1;

  for (n = 0 ; ws < ws_end ; ++ws)
    if (bt_search(*ws, j2u, &wc1) != bt_failure) {
      *ws = wc1;
      ++n;
    }

  return n;
}

static unsigned int u2j[] = {
#include "ucs-to-jis0208-extra.h"
};

size_t
mb_ucs_to_jisx0208_extra(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
  size_t n;
  unsigned int ucs, wc1;

  for (n = 0 ; ws < ws_end ; ++ws) {
    if (*ws & MB_NON_UCS_MARK) {
      mb_wchar_t wc;

      wc = *ws;

      if (!mb_conv_to_ucs(&wc, &wc + 1, info))
	continue;

      ucs = wc;
    }
    else
      ucs = *ws;

    if (bt_search(ucs, u2j, &wc1) != bt_failure) {
      *ws = wc1;
      ++n;
    }
  }

  return n;
}
