#ifdef USE_KOI8R
defces(KOI8R, KOI8R)
#endif
#ifdef USE_KOI8U
defces(KOI8U, KOI8U)
#endif
#ifdef USE_WIN1250
defces(WIN1250, WIN1250)
#endif
#ifdef USE_WIN1251
defces(WIN1251, WIN1251)
#endif
#ifdef USE_WIN1252
defces(WIN1252, WIN1252)
#endif
#ifdef USE_WIN1253
defces(WIN1253, WIN1253)
#endif
#ifdef USE_WIN1254
defces(WIN1254, WIN1254)
#endif
#ifdef USE_WIN1255
defces(WIN1255, WIN1255)
#endif
#ifdef USE_WIN1256
defces(WIN1256, WIN1256)
#endif
#ifdef USE_WIN1257
defces(WIN1257, WIN1257)
#endif
#ifdef USE_WIN1258
defces(WIN1258, WIN1258)
#endif
#ifdef USE_SJIS
defces(SJIS, SJIS)
#endif
#ifdef USE_SJIS0213
defces(SJIS0213, SJIS0213)
#endif
#ifdef USE_BIG5
defces(BIG5, BIG5)
#endif
#ifdef USE_JOHAB
defces(JOHAB, JOHAB)
#endif
#ifdef USE_UHANG
defces(UHANG, UHANG)
#endif
#ifdef USE_EUC_TW
defces(EUC_TW, EUC_TW)
#endif
#ifdef USE_EUC_JISX0213_PACKED
defces(EUC_JISX0213_PACKED, EUC_JISX0213_PACKED)
#endif
#ifdef USE_GBK
defces(GBK, GBK)
#endif
#ifdef USE_GBK2K
defces(GBK2K, GBK2K)
#endif
