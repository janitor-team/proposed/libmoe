BEGIN {require 'mbcesdefs.pl';}

printf "MB_94_LOWER==0x%X, MB_94_UPPER==0x%X\n", MB_94_LOWER, MB_94_UPPER;
printf "MB_96_LOWER==0x%X, MB_96_UPPER==0x%X\n", MB_96_LOWER, MB_96_UPPER;
printf "MB_SBC_LOWER==0x%X, MB_SBC_UPPER==0x%X\n", MB_SBC_LOWER, MB_SBC_UPPER;
printf "MB_94x94_LOWER==0x%X, MB_94x94_UPPER==0x%X\n", MB_94x94_LOWER, MB_94x94_UPPER;
printf "MB_DBC_LOWER==0x%X, MB_DBC_UPPER==0x%X\n", MB_DBC_LOWER, MB_DBC_UPPER;

for (@ARGV) {
  printf "%s==0x%X\n", $_, eval($_);
}
