#include "mblangconf.h"

%%BEGIN default_detector_tab

/* C */
#ifdef USE_CN
"c",&mb_cesv_CN
"cn",&mb_cesv_CN
"china",&mb_cesv_CN
"chinese",&mb_cesv_CN
#endif

/* J */
#ifdef USE_JA
"j",&mb_cesv_JA
"ja",&mb_cesv_JA
"jp",&mb_cesv_JA
"japan",&mb_cesv_JA
"japanese",&mb_cesv_JA
#endif

/* K */
#ifdef USE_KR
"k",&mb_cesv_KR
"ko",&mb_cesv_KR
"kr",&mb_cesv_KR
"korea",&mb_cesv_KR
"korean",&mb_cesv_KR
#endif

/* and ALL */
"cjk",&mb_cesv_CJK
