use UNIVERSAL qw(isa);

BEGIN {require 'mbcesdefs.pl'}

my @mb_misc_to_ucs =
  (
   'koi8-r',
   mb_SBC,
   MB_KOI8R_FC,
   [
    0x2500,0x2502,0x250c,0x2510,0x2514,0x2518,0x251c,0x2524,
    0x252c,0x2534,0x253c,0x2580,0x2584,0x2588,0x258c,0x2590,
    0x2591,0x2592,0x2593,0x2320,0x25a0,0x2219,0x221a,0x2248,
    0x2264,0x2265,0x00a0,0x2321,0x00b0,0x00b2,0x00b7,0x00f7,
    0x2550,0x2551,0x2552,0x0451,0x2553,0x2554,0x2555,0x2556,
    0x2557,0x2558,0x2559,0x255a,0x255b,0x255c,0x255d,0x255e,
    0x255f,0x2560,0x2561,0x0401,0x2562,0x2563,0x2564,0x2565,
    0x2566,0x2567,0x2568,0x2569,0x256a,0x256b,0x256c,0x00a9,
    0x044e,0x0430,0x0431,0x0446,0x0434,0x0435,0x0444,0x0433,
    0x0445,0x0438,0x0439,0x043a,0x043b,0x043c,0x043d,0x043e,
    0x043f,0x044f,0x0440,0x0441,0x0442,0x0443,0x0436,0x0432,
    0x044c,0x044b,0x0437,0x0448,0x044d,0x0449,0x0447,0x044a,
    0x042e,0x0410,0x0411,0x0426,0x0414,0x0415,0x0424,0x0413,
    0x0425,0x0418,0x0419,0x041a,0x041b,0x041c,0x041d,0x041e,
    0x041f,0x042f,0x0420,0x0421,0x0422,0x0423,0x0416,0x0412,
    0x042c,0x042b,0x0417,0x0428,0x042d,0x0429,0x0427,0x042a,
    ],

   'koi8-u',
   mb_SBC,
   MB_KOI8U_FC,
   [
    0x2500,0x2502,0x250C,0x2510,0x2514,0x2518,0x251C,0x2524,
    0x252C,0x2534,0x253C,0x2580,0x2584,0x2588,0x258C,0x2590,
    0x2591,0x2592,0x2593,0x2320,0x25A0,0x2219,0x221A,0x2248,
    0x2264,0x2265,0x00A0,0x2321,0x00B0,0x00B2,0x00B7,0x00F7,
    0x2550,0x2551,0x2552,0x0451,0x0454,0x2554,0x0456,0x0457,
    0x2557,0x2558,0x2559,0x255A,0x255B,0x0491,0x255D,0x255E,
    0x255F,0x2560,0x2561,0x0401,0x0404,0x2563,0x0406,0x0407,
    0x2566,0x2567,0x2568,0x2569,0x256A,0x0490,0x256C,0x00A9,
    0x044E,0x0430,0x0431,0x0446,0x0434,0x0435,0x0444,0x0433,
    0x0445,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,
    0x043F,0x044F,0x0440,0x0441,0x0442,0x0443,0x0436,0x0432,
    0x044C,0x044B,0x0437,0x0448,0x044D,0x0449,0x0447,0x044A,
    0x042E,0x0410,0x0411,0x0426,0x0414,0x0415,0x0424,0x0413,
    0x0425,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,
    0x041F,0x042F,0x0420,0x0421,0x0422,0x0423,0x0416,0x0412,
    0x042C,0x042B,0x0417,0x0428,0x042D,0x0429,0x0427,0x042A,
    ],
   );

my @supplement =
  (
   [0xFF3C, mb_94x94, 0x42, 0x21 - 0x21, 0x40 - 0x21, 'jix0208'], # FULLWIDTH REVERSE SOLIDUS
   );

my (@map, @notascii_map, %jis1tab, %jis1map);

sub dbc2c {$_[0] * 94 + $_[1];}

sub set_map {
  my ($ucs, $set, $fc, $c1, $c2, $cmt) = @_;
  my $c = &mb_word_enc($set, $fc, &dbc2c($c1, $c2));

  &do_set_map($ucs, $c, $cmt);
  &do_set_map($ucs, $c, $cmt, \@notascii_map) if ($set == mb_94 && $fc != MB_ASCII_FC);
}

sub do_set_map {
  my ($ucs, $c, $cmt, $map, $force) = @_;

  if ($ucs >= 0x80) {
    $map = \@map if (!defined($map));
    $map->[$ucs] = +{} if (ref($map->[$ucs]) ne 'HASH');
    $map->[$ucs]->{$c} = $cmt if ($force || $map->[$ucs]->{$c} eq '');
  }
}

sub sbc96 {
  my ($iso, $ucs) = map{hex($_)} split(/\s+/, $_[0]);

  $iso >= 0xA0 && &set_map($ucs, mb_96, $_[1], 0, $iso - 0xA0, @_[2 .. $#_]);
}

sub uni2cns {
  if ($_[0] =~ /^[0-9A-Fa-f]{4}\t/) {
    my ($ucs, @cns) = grep(!/^\#/, split(/\s+/, $_[0]));
    my %plane;

    $ucs = hex($ucs);

    foreach (grep(/^[1-7]-/, @cns)) {
      my ($p, $c) = split(/-/, $_, 2);

      unless ($plane{$p}) {
	$plane{$p} = 1;
	&set_map($ucs, mb_94x94, 0x47 + $p - 1,
		 (map {(hex($_) & 0x7F) - 0x21} ($c =~ /^([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})$/)),
		 "cns11643-$p");
      }
    }
  }
}

sub uni2gb {
  if ($_[0] =~ /^[0-9A-Fa-f]{4}\t/) {
    my ($ucs, @gb) = grep(!/^\#/, split(/\s+/, $_[0]));

    foreach (grep(/^[0S]-/, @gb)) {
      my ($which, $c) = split(/-/, $_, 2);

      &set_map(hex($ucs), mb_94x94,
	       $which eq '0' ? 0x41 : 0x45,
	       (map {$_ - 1} ($c =~ /^(\d{2})(\d{2})$/)),
	       $which eq '0' ? 'gb2312' : 'gb8565.2');
    }
  }
}

sub uni2gbk2k {
  if ($_[0] =~ /<a\s+u=\"([^\"]+)\"\s+b=\"([^\"]+)\"/) {
    my $ucs = hex($1);
    my $mbc = $2;

    if ($ucs >= 0x80) {
      my $enc = &MB_GBK2K_ENC(map {hex($_)} $mbc =~ /([0-9A-Fa-f]{2})/g);

      &do_set_map($ucs, $enc, 'gbk2k') if (defined($enc));
    }
  }
}

sub uni2big5 {
  if ($_[0] =~ /^0x([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})\s+0x([0-9A-Fa-f]{4})/) {
    my ($c1, $c2, $ucs) = (hex($1), hex($2), hex($3));
    my $enc = &mb_BIG5_enc($c1, $c2);

    &do_set_map($ucs, $enc, 'big5') if (defined($enc));
  }
}

my %fc = ('0' => 0x40, '2' => 0x42);

sub jis1reg {
  my ($flag, $row, $col, $ucs) = @_;
  my $key = sprintf('%X-%X', $row * 94 + $col, $ucs);

  $jis1tab{$key} |= $flag;
}

sub uni2jis {
  if ($_[0] =~ /^[0-9A-Fa-f]{4}[ \t]/) {
    my ($ucs, @jis) = grep(!/^\#/, split(/\s+/, $_[0]));

    $ucs = hex($ucs);

    foreach (@jis) {
      if (/^([02])-(\d{2})(\d{2})$/) {
	&jis1reg($1 eq '0' ? MB_IN_JISC6226 : MB_IN_JISX0208, $2 - 1, $3 - 1, $ucs);
      }
      elsif (/^S-(\d{2})(\d{2})$/) {
	&set_map($ucs, mb_94x94, 0x44, $1 - 1, $2 - 1, $ucs);
      }
      elsif (m%^([KR])-(\d+)/(\d+)$%) {
	my $c = ($2 * 0x10 + $3);

	&set_map($ucs, mb_94, $1 eq 'K' ? 0x49 : 0x4A, 0, $c - 0x21, $1 eq 'K' ? 'jisx0201-r' : 'jisx0201-l')
	  if ($c >= 0x21 && $c <= 0x7E);
      }
    }
  }
}

sub uni2jisx0213 {
  if ($_[0] =~ /^u-([0-9A-Fa-f]{4}|0)\s+(\d)-(\d+)-(\d+)/) {
    my $ucs = hex($1);

    if ($2 eq '1') {
      &jis1reg(MB_IN_JISX0213_1, $3 - 1, $4 - 1, $ucs);
    }
    else {
      &set_map($ucs, mb_94x94, 0x50, $3 - 1, $4 - 1, 'jisx0213-2');
    }
  }
}

sub mkjis1map () {
  my ($jis, $ucs, $key, $flag);

  while (($key, $flag) = each %jis1tab) {
    ($jis, $ucs) = map {hex($_)} split(/-/, $key);

    if ($ucs && $ucs != 0xFFFF) {
      if ($flag & MB_IN_JISX0213_1) {
	&do_set_map($ucs, &mb_word_94x94_enc(0x4F, $jis), 'jisx0213-1');
	$jis1map{$jis} |= $flag if ($flag != MB_IN_JIS1COMMON);
      }
      else {
	$jis1map{$jis} &= ~$flag;
      }

      &do_set_map($ucs, &mb_word_94x94_enc(0x40, $jis), 'jisc6226') if ($flag & MB_IN_JISC6226);
      &do_set_map($ucs, &mb_word_94x94_enc(0x42, $jis), 'jisx0208') if ($flag & MB_IN_JISX0208);
    }
  }
}

sub uni2ksx {
  my ($c1, $c2, $ucs) = $_[0] =~ /^0x([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})\s+0x([0-9A-Fa-f]{4})/;

  &set_map(hex($ucs), mb_94x94, 0x43, hex($c1) - 0x21, hex($c2) - 0x21, 'ksx1001') if (defined($ucs));
}

sub uni2johab {
  if ($_[0] =~ /^0x([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})\s+0x([0-9A-Fa-f]{4})/) {
    my ($c1, $c2, $ucs) = (hex($1), hex($2), hex($3));
    my $enc = &mb_JOHAB_enc($c1, $c2);

    &do_set_map($ucs, $enc, 'johab') if (defined($enc) &&
					 $enc >= &mb_word_DBC_enc(MB_JOHAB_FC, 0) &&
					 $enc <= &mb_word_DBC_enc(MB_JOHAB_FC, MB_DBC_UNIT - 1));
  }
}

sub uni2uhang {
  if ($_[0] =~ /^0x([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})\s+0x([0-9A-Fa-f]{4})/) {
    my ($c1, $c2, $ucs) = (hex($1), hex($2), hex($3));
    my $enc = &mb_UHANG_enc($c1, $c2);

    &do_set_map($ucs, $enc, 'uhang') if (defined($enc) &&
					 $enc >= &mb_word_DBC_enc(MB_UHANG_FC, 0) &&
					 $enc <= &mb_word_DBC_enc(MB_UHANG_FC, MB_DBC_UNIT - 1));
  }
}

sub unihan {
  my ($ucs, $which, $code) = $_[0] =~ /^u\+([0-9A-Fa-f]+)\s+(\S+)\s+(\S+)/i;

  $ucs = hex($ucs);

  if ($which eq 'kJIS0213') {
    if ($code =~ /^([12]),(\d{2}),(\d{2})/) {
      if ($1 eq '1') {
	&jis1reg(MB_IN_JISX0213_1, $2 - 1, $3 - 1, $ucs);
      }
      else {
	&set_map($ucs, mb_94x94, 0x50, $2 - 1, $3 - 1, 'jisx0213-2');
      }
    }
  }
  elsif ($which eq 'kIRG_JSource') {
    if ($code =~ /^([34])-([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})/) {
      if ($1 eq '3') {
	&jis1reg(MB_IN_JISX0213_1, hex($2) - 0x21, hex($3) - 0x21, $ucs);
      }
      else {
	&set_map($ucs, mb_94x94, 0x50, hex($2) - 0x21, hex($3) - 0x21, 'jisx0213-2');
      }
    }
  }
  elsif ($which eq 'kIRG_TSource') {
    if ($code =~ /^([1-7Ff])-([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})/) {
      my $p = hex($1);
      my $enc = &mb_EUC_TW_enc(0x8E, 0xA0 + $p, hex($2), hex($3));

      &do_set_map($ucs, $enc, 'cns11643-' . $p) if (defined($enc));
    }
  }
}

sub win125x {
  if ($_[2] =~ /^([8-9A-Fa-f][0-9A-Fa-f])\s*=\s*U\+([0-9A-Fa-f]{4})\s*:/i) {
    my ($c1, $ucs) = (hex($1), hex($2));
    my $enc = &{$_[0]}($c1);

    &do_set_map($ucs, $enc, $_[1]) if (defined($enc));
  }
}

sub make_map {
  my $info = shift;
  my ($line, $i, $func);

  while (defined($line = <>)) {
    if ($line =~ s/^\s*\#\s*Name:\s*//i) {
      if ($line =~ m%ISO.*8859-(\d+)%i) {
	my $id = "ISO8859_$1";
	my $ces = CES->by_id($id);

	if (isa($ces, 'CES')) {
	  my $cmt = "iso8859-$1";
	  my $fc = $ces->G->[3]->[1];

	  $info->($line);
	  $func = sub {&sbc96($_[0], $fc, $cmt);};
	}
	else {
	  $func = undef;
	}
      }
      elsif ($line =~ /^The\s+Unicode\s+Han\s+Character\s+Cross-Reference/i) {
	$func = undef;
      }
      elsif ($line =~ /\bCNS/i) {
	$info->($line);
	$func = \&uni2cns;
      }
      elsif ($line =~ /\bGB\b/i) {
	$info->($line);
	$func = \&uni2gb;
      }
      elsif ($line =~ /\bBig\s*(5|Five)\b/i) {
	$info->($line);
	$func = \&uni2big5;
      }
      elsif ($line =~ /\bJIS\s*X\s*0213\b/i) {
	$info->($line);
	$func = \&uni2jisx0213;
      }
      elsif ($line =~ /\bJIS\b/i) {
	$info->($line);
	$func = \&uni2jis;
      }
      elsif ($line =~ /\bKS\s*X\s*1001\b/i) {
	$info->($line);
	$func = \&uni2ksx;
      }
      elsif ($line =~ /\bJohab\b/i) {
	$info->($line);
	$func = \&uni2johab;
      }
      elsif ($line =~ /\bUnified\s*Hange?ul\b/i) {
	$info->($line);
	$func = \&uni2uhang;
      }
      elsif ($line =~ /\bUnihan\s+database\b/i) {
	$info->($line);
	$func = \&unihan;
      }
      else {
	$func = undef;
      }
    }
    elsif ($line =~ /^Korean\s+Hangul\s+Encoding\s+Conversion\s+Table/i) {
      $func = undef;
    }
    elsif ($line =~ /^Microsoft\s+Windows\s+Code\s*page\s*:\s*(125[0-8])\s/i) {
      my $cmt = "windows-$1";
      my $encoder = \&{'mb_WIN' . $1 . '_enc'};

      $info->($line);
      $func = sub {&win125x($encoder, $cmt, @_)};
    }
    elsif ($line =~ s/^<characterMapping\s+id=\"([^\"]+)\".*/$1/) {
      if ($line =~ /\bgb-18030-2000\b/i) {
	$info->($line);
	$func = \&uni2gbk2k;
      }
      else {
	$func = undef;
      }
    }
    elsif ($line !~ /^\#/ && ref($func) eq 'CODE') {
      $func->($line);
    }
  }

  $info->('TIS-620');

  &set_map(0xA0, mb_96, 0x54, 0, 0, 'tis-620');

  my $tis_c;

  foreach $tis_c (((0xA1 .. 0xDA), (0xDF .. 0xFB))) {
    &set_map(0xE01 + $tis_c - 0xA1, mb_96, 0x54, 0, $tis_c - 0xA0, 'tis-620');
  }

  $info->("Supplement");

  my $argv;

  foreach $argv (@supplement) {
    &set_map(@$argv);
  }

  my ($set, $fc, $c, $cmt, $arr);

  for ($i = 3 ; $i < @mb_misc_to_ucs ; $i += 4) {
    ($cmt, $set, $fc, $arr) = @mb_misc_to_ucs[$i - 3, $i - 2, $i - 1, $i];
    $info->($cmt);

    for ($c = 0 ; $c < @$arr ; ++$c) {
      &do_set_map($arr->[$c], &mb_word_enc($set, $fc, $c), $cmt);
    }
  }

  $info->("JIS map");

  &mkjis1map;
  (\@map, \@notascii_map, \%jis1map);
}

1;
