#include "mbcesconf.h"
#include "mblangconf.h"
%%TYPE number
%%BEGIN
#ifdef USE_JA
0x3001-0x3001,6U
#endif
#ifdef USE_JA
0x3002-0x3002,6U
#endif
#ifdef USE_JA
0x3041-0x3093,4U
#endif
#ifdef USE_JA
0x30A1-0x30FE,2U
#endif
#ifdef USE_CN
0x4E5F-0x4E5F,10U
#endif
#ifdef USE_CN
0x5728-0x5728,10U
#endif
#ifdef USE_CN
0x662F-0x662F,10U
#endif
#ifdef USE_CN
0x7684-0x7684,10U
#endif
#ifdef USE_KR
0xAC00-0xAC00,2U
#endif
#ifdef USE_KR
0xACE0-0xACE0,2U
#endif
#ifdef USE_KR
0xAE30-0xAE30,2U
#endif
#ifdef USE_KR
0xB294-0xB294,4U
#endif
#ifdef USE_KR
0xB2E4-0xB2E4,6U
#endif
#ifdef USE_KR
0xB370-0xB370,2U
#endif
#ifdef USE_KR
0xB85C-0xB85C,2U
#endif
#ifdef USE_KR
0xB97C-0xB97C,2U
#endif
#ifdef USE_KR
0xB9AC-0xB9AC,2U
#endif
#ifdef USE_KR
0xBE44-0xBE44,2U
#endif
#ifdef USE_KR
0xC0AC-0xC0AC,2U
#endif
#ifdef USE_KR
0xC11C-0xC11C,2U
#endif
#ifdef USE_KR
0xC2A4-0xC2A4,2U
#endif
#ifdef USE_KR
0xC548-0xC548,2U
#endif
#ifdef USE_KR
0xC5B4-0xC5B4,2U
#endif
#ifdef USE_KR
0xC5D0-0xC5D0,2U
#endif
#ifdef USE_KR
0xC740-0xC740,2U
#endif
#ifdef USE_KR
0xC744-0xC744,2U
#endif
#ifdef USE_KR
0xC758-0xC758,2U
#endif
#ifdef USE_KR
0xC774-0xC774,6U
#endif
#ifdef USE_KR
0xC788-0xC788,2U
#endif
#ifdef USE_KR
0xC790-0xC790,2U
#endif
#ifdef USE_KR
0xC9C0-0xC9C0,2U
#endif
#ifdef USE_KR
0xD2B8-0xD2B8,2U
#endif
#ifdef USE_KR
0xD558-0xD558,4U
#endif
#ifdef USE_KR
0xD55C-0xD55C,2U
#endif
#ifdef USE_JA
0xFF0C-0xFF0C,6U
#endif
#ifdef USE_JA
0xFF0E-0xFF0E,6U
#endif
#ifdef USE_CN
0x2167FF-0x2167FF,10U
#endif
#ifdef USE_CN
0x216FB8-0x216FB8,10U
#endif
#ifdef USE_CN
0x217293-0x217293,10U
#endif
#ifdef USE_CN
0x217377-0x217377,10U
#endif
#ifdef USE_JA
0x218309-0x218309,6U
#endif
#ifdef USE_JA
#ifdef USE_SJIS
0x218309-0x218309,6U
#endif
#endif
#ifdef USE_JA
0x21830A-0x21830A,6U
#endif
#ifdef USE_JA
#ifdef USE_SJIS
0x21830A-0x21830A,6U
#endif
#endif
#ifdef USE_JA
#ifdef USE_SJIS
0x21830B-0x21830B,6U
#endif
#endif
#ifdef USE_JA
0x21830B-0x21830B,6U
#endif
#ifdef USE_JA
#ifdef USE_SJIS
0x21830C-0x21830C,6U
#endif
#endif
#ifdef USE_JA
0x21830C-0x21830C,6U
#endif
#ifdef USE_JA
0x218422-0x218474,4U
#endif
#ifdef USE_JA
#ifdef USE_SJIS
0x218422-0x218474,4U
#endif
#endif
#ifdef USE_JA
0x218480-0x2184D5,2U
#endif
#ifdef USE_JA
#ifdef USE_SJIS
0x218480-0x2184D5,2U
#endif
#endif
#ifdef USE_KR
0x21AB0E-0x21AB0E,2U
#endif
#ifdef USE_KR
0x21AB5A-0x21AB5A,2U
#endif
#ifdef USE_KR
0x21ABAD-0x21ABAD,2U
#endif
#ifdef USE_KR
0x21ACA7-0x21ACA7,4U
#endif
#ifdef USE_KR
0x21ACBE-0x21ACBE,6U
#endif
#ifdef USE_KR
0x21ACE8-0x21ACE8,2U
#endif
#ifdef USE_KR
0x21ADCD-0x21ADCD,2U
#endif
#ifdef USE_KR
0x21AE03-0x21AE03,2U
#endif
#ifdef USE_KR
0x21AE0B-0x21AE0B,2U
#endif
#ifdef USE_KR
0x21AF0A-0x21AF0A,2U
#endif
#ifdef USE_KR
0x21AF5E-0x21AF5E,2U
#endif
#ifdef USE_KR
0x21AF82-0x21AF82,2U
#endif
#ifdef USE_KR
0x21AFED-0x21AFED,2U
#endif
#ifdef USE_KR
0x21B059-0x21B059,2U
#endif
#ifdef USE_KR
0x21B07F-0x21B07F,2U
#endif
#ifdef USE_KR
0x21B090-0x21B090,2U
#endif
#ifdef USE_KR
0x21B107-0x21B107,2U
#endif
#ifdef USE_KR
0x21B108-0x21B108,2U
#endif
#ifdef USE_KR
0x21B114-0x21B114,2U
#endif
#ifdef USE_KR
0x21B119-0x21B119,6U
#endif
#ifdef USE_KR
0x21B123-0x21B123,2U
#endif
#ifdef USE_KR
0x21B127-0x21B127,2U
#endif
#ifdef USE_KR
0x21B1A1-0x21B1A1,2U
#endif
#ifdef USE_KR
0x21B32F-0x21B32F,2U
#endif
#ifdef USE_KR
0x21B3AE-0x21B3AE,4U
#endif
#ifdef USE_KR
0x21B3B0-0x21B3B0,2U
#endif
#ifdef USE_CN
#ifdef USE_EUC_TW
0x223C93-0x223C93,10U
#endif
#endif
#ifdef USE_CN
#ifdef USE_EUC_TW
0x223DD2-0x223DD2,10U
#endif
#endif
#ifdef USE_CN
#ifdef USE_EUC_TW
0x22407C-0x22407C,10U
#endif
#endif
#ifdef USE_CN
#ifdef USE_EUC_TW
0x22416D-0x22416D,10U
#endif
#endif
#ifdef USE_CN
#ifdef USE_BIG5
0x3381F4-0x3381F4,10U
#endif
#endif
#ifdef USE_CN
#ifdef USE_BIG5
0x338333-0x338333,10U
#endif
#endif
#ifdef USE_CN
#ifdef USE_BIG5
0x3385DD-0x3385DD,10U
#endif
#endif
#ifdef USE_CN
#ifdef USE_BIG5
0x3386CE-0x3386CE,10U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x340310-0x340310,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x34040A-0x34040A,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x340582-0x340582,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x340B28-0x340B28,4U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x340BE0-0x340BE0,6U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x340C7C-0x340C7C,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3412BA-0x3412BA,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3413FC-0x3413FC,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x341432-0x341432,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x341A12-0x341A12,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x341D80-0x341D80,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x341DFE-0x341DFE,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x341FB4-0x341FB4,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x342364-0x342364,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3423DE-0x3423DE,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3423FC-0x3423FC,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x342598-0x342598,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x34259C-0x34259C,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3425B2-0x3425B2,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3425D2-0x3425D2,6U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3425E7-0x3425E7,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x342650-0x342650,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3428C2-0x3428C2,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x343444-0x343444,2U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3437F0-0x3437F0,4U
#endif
#endif
#ifdef USE_KR
#ifdef USE_JOHAB
0x3437F4-0x3437F4,2U
#endif
#endif
